package com.knowledge.apps.hazelcast.config;

import javax.annotation.PreDestroy;

import com.hazelcast.core.HazelcastInstance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HazelcastShutdownConfig {

    @Autowired
    public HazelcastInstance hazelcastInstance;

    @PreDestroy
    public void shutdownHazelcast() {
        hazelcastInstance.shutdown();
    }
    
}
