package com.knowledge.apps.hazelcast.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "hazelcast")
public class HazelcastProperties {

    private String serviceDns;
    private int serviceDnsTimeout;
    private String terminationPolicy;
    private String shutdownMaxWait;
    private List<HazelcastQueueProperties> queues;

    public String getServiceDns() {
        return serviceDns;
    }

    public void setServiceDns(String serviceDns) {
        this.serviceDns = serviceDns;
    }

    public int getServiceDnsTimeout() {
        return serviceDnsTimeout;
    }

    public void setServiceDnsTimeout(int serviceDnsTimeout) {
        this.serviceDnsTimeout = serviceDnsTimeout;
    }

    public String getTerminationPolicy() {
        return terminationPolicy;
    }

    public void setTerminationPolicy(String terminationPolicy) {
        this.terminationPolicy = terminationPolicy;
    }

    public String getShutdownMaxWait() {
        return shutdownMaxWait;
    }

    public void setShutdownMaxWait(String shutdownMaxWait) {
        this.shutdownMaxWait = shutdownMaxWait;
    }

    public List<HazelcastQueueProperties> getQueues() {
        return queues;
    }

    public void setQueues(List<HazelcastQueueProperties> queues) {
        this.queues = queues;
    }

    
    
}
