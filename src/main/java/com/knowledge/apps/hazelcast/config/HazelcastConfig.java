package com.knowledge.apps.hazelcast.config;

import static com.hazelcast.kubernetes.KubernetesProperties.SERVICE_DNS;
import static com.hazelcast.kubernetes.KubernetesProperties.SERVICE_DNS_TIMEOUT;

import java.util.List;

import com.hazelcast.config.Config;
import com.hazelcast.config.DiscoveryStrategyConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.kubernetes.HazelcastKubernetesDiscoveryStrategyFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class HazelcastConfig {

    private static final String DISCOVERY_ENABLED = "hazelcast.discovery.enabled";
    
    /**  
     * @deprecated
     * This system property is deprecated in version 4.0.x.
     * Use {@link RestApiConfig} instead
    */
    @Deprecated
    private static final String REST_ENABLED = "hazelcast.rest.enabled";

    /** 
     * @deprecated
     * This system property is deprecated in version 4.0.x.
     * Use {@link RestApiConfig} instead.
    */
    @Deprecated
    private static final String HEALTHCHECK_ENABLED = "hazelcast.http.healthcheck.enabled";
    
    private static final String TERMINATION_POLICY = "hazelcast.shutdownhook.policy";
    private static final String SHUTDOWN_MAX_WAIT = "hazelcast.graceful.shutdown.max.wait";

    private final Logger logger = LoggerFactory.getLogger(getClass());


    private Config buildQueues(final Config config, final List<HazelcastQueueProperties> queueProperties) {
        
        queueProperties.forEach(q -> config.getQueueConfig(q.getName())
                .setName(q.getName())
                .setMaxSize(q.getMaxSize())
                .setAsyncBackupCount(q.getBackupCount())
                .setStatisticsEnabled(q.isStatisticsEnabled())
        );
        
        return config;
    }
    
    /** 
     * Build a Hazelcast {@link Config} object for use locally.
     * 
     * @param hazelcastProperties Configuration for Hazelcast.
     * @return A Hazelcast {@link Config} object.
     */
    @Profile("dev")
    @Bean
    public Config localHazelcast(final HazelcastProperties hazelcastProperties) {

        final Config config = new Config();
        final JoinConfig joinConfig = config.getNetworkConfig().getJoin();

        config.setProperty(REST_ENABLED, "true")
                .setProperty(HEALTHCHECK_ENABLED, "true")
                .setProperty(TERMINATION_POLICY, hazelcastProperties.getTerminationPolicy())
                .setProperty(SHUTDOWN_MAX_WAIT, hazelcastProperties.getShutdownMaxWait());

        joinConfig.getMulticastConfig()
                .setEnabled(false);

        joinConfig.getTcpIpConfig()
                .setEnabled(true);

        buildQueues(config, hazelcastProperties.getQueues());

        logger.info("Returning Hazelcast Config: {}", config);
        
        return config;
    }

     /** 
     * Build a Hazelcast {@link Config} object for use with a {@link com.hazelcast.core.HazelcastInstance}.
     * The object returned will configure Hazelcast for deployment on Kubernetes/Openshift
     * by using the HazelcastKubernetesDiscoveryStrategy and using Kubernetes/Openshift
     * service DNS for discovery. 
     * 
     * @param hazelcastProperties Configuration for Hazelcast.
     * @return A Hazelcast {@link Config} object for Kubernetes/OpenShift.
     */
    @Profile("!dev")
    @Bean
    public Config hazelcast(final HazelcastProperties hazelcastProperties) {

        final Config config = new Config();
        final NetworkConfig networkConfig = config.getNetworkConfig();
        final JoinConfig joinConfig = networkConfig.getJoin();

        final DiscoveryStrategyConfig kubernetesDiscovery = new DiscoveryStrategyConfig(
                new HazelcastKubernetesDiscoveryStrategyFactory()
        );

        config.setProperty(DISCOVERY_ENABLED, "true")
                .setProperty(REST_ENABLED, "true")
                .setProperty(HEALTHCHECK_ENABLED, "true")
                .setProperty(TERMINATION_POLICY, hazelcastProperties.getTerminationPolicy())
                .setProperty(SHUTDOWN_MAX_WAIT, hazelcastProperties.getShutdownMaxWait());

        networkConfig.setPort(5701);
        networkConfig.setPortAutoIncrement(true);

        kubernetesDiscovery.addProperty(SERVICE_DNS.key(), hazelcastProperties.getServiceDns());
        kubernetesDiscovery.addProperty(SERVICE_DNS_TIMEOUT.key(), hazelcastProperties.getServiceDnsTimeout());
        
        joinConfig.getMulticastConfig()
                .setEnabled(false);

        joinConfig.getTcpIpConfig()
                .setEnabled(false);

        joinConfig.getDiscoveryConfig()
                .addDiscoveryStrategyConfig(kubernetesDiscovery);

        buildQueues(config, hazelcastProperties.getQueues());

        logger.info("Returning Hazelcast Config: {}", config);
        
        return config;
    }
    
}
