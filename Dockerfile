# Build from maven
FROM maven:latest AS build

# Copy our application in
COPY . /hazelcast

# Create local .m2 directory for maven
RUN mkdir -p /root/.m2 \
    && mkdir /root/.m2/repository

# Copy maven settings, containing repository configurations
COPY ./configuration/settings.xml /root/.m2

# change the working directory to where we are building the application
WORKDIR /hazelcast

# Package the app with maven
RUN mvn clean package

# Create new build stage from Java image
FROM fabric8/s2i-java:latest-java11

# Copy artifact from first stage into working directory 
COPY --from=build /hazelcast /hazelcast

# Change to directory of app
WORKDIR /hazelcast/target

# Entry point for app
ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar", "hazelcast-0.0.1-SNAPSHOT.jar"]